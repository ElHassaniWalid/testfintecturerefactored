
const config = require('../config.json');
const port = process.env.PORT || config.dev.defaultPort;
const url= "http://localhost:"+port;
let views = {
    indexView : (useCases)=>{
        let view = "<div>"
                    +"<h1>Use Cases List</h1><ul>";
        for (let i = 0; i < useCases.length; i++) {
            let row = useCases[i];
            view += "<li><a href='"+url+"/users/accounts/"+row.code+"'>"+row.name+" ("+row.description+")</a></li>";
        }
        view += "</ul>";
        return view;
    },

    accountsView : (data)=>{
        let view = "<div>"
                    +"<h1>Accounts List</h1><ul>";
        if(data === undefined) return "No data ! </div>";
        idUseCase = data.idUseCase;
        data = data.data;
        
        if(data.accounts === undefined || data.accounts === 0) return "No Accounts ! </div>";
        accounts = data.accounts;
        view +="<table><tr><th>bicFi</th><th>Name</th><th>Transactions</th><th>Balances</th></tr>";
        for (let i = 0; i < accounts.length; i++) {
            let row = accounts[i];
            view += "<tr><td>"+row.bicFi+"</td><td>"+row.name+"</td><td><a href='"+url+"/users/accounts/"+idUseCase+"/transactions/"+row.resourceId+"'>"+row.resourceId+" Transactions</a></td><td><a href='"+url+"/users/accounts/"+idUseCase+"/balances/"+row.resourceId+"'>"+row.resourceId+" Balances</a></td></tr>";
        }
        view += "</table>";
        return view;
    },


    balancesView : (data)=>{
        let view = "<div>"
                    +"<h1>Balances List</h1><ul>";
        if(data === undefined) return "No data ! </div>";
        idUseCase = data.idUseCase;
        data = data.data;
        
        if(data.balances === undefined || data.balances === 0) return "No Balance ! </div>";
        balances = data.balances;
        view +="<table><tr><th>Name</th><th>Balance Amount</th><th>Balance Type</th><th>Date</th></tr>";
        for (let i = 0; i < balances.length; i++) {
            let row = balances[i];
            view += "<tr><td>"+row.name+"</td><td>"+row.balanceAmount.amount+" "+row.balanceAmount.currency+"</td><td>"+row.balanceType+"</td><td>"+row.referenceDate+"</td></tr>";
        }
        view += "</table>";
        return view;
    },

    transactionsView : (data)=>{
        let view = "<div>"
                    +"<h1>Transactions List</h1><ul>";
        if(data === undefined) return "No data ! </div>";  
        idUseCase = data.idUseCase;
        data = data.data;
        
        if(data.transactions === undefined || data.transactions === 0) return "No Transaction ! </div>";
        transactions = data.transactions;
        view +="<table><tr><th>Transaction Amount</th><th>status</th><th>Date transaction</th></tr>";
        for (let i = 0; i < transactions.length; i++) {
            let row = transactions[i];
            view += "<tr><td>"+row.transactionAmount.amount+" "+row.transactionAmount.currency+"</td><td>"+row.status+"</td><td>"+row.bookingDate+"</td></tr>";
        }
        view += "</table>";
        return view;
    }
}

module.exports = views;