const express = require('express');
const app = express();
const morgan = require('morgan');
const session = require('express-session');
const config = require('./config.json');

const port = process.env.PORT || config.dev.defaultPort;

app.use(express.json());
app.use(morgan('dev'));
app.use(session({
    secret: 'J9Y89Y3j.JUDO',
    resave: false, 
    saveUninitialized: false
  }));

app.use("/users", require("./routes/users"));

app.get('/', function (req, res) {
  res.status(200).send('Welcome Welcome !!!');
});

app.listen(port, ()=>{
  console.log("Listening on port "+ port +" ...");
});