const express = require('express');
const router = require('express-promise-router')();

const usersController = require('../controllers/usersController');

router.route('/')
    .get(
        usersController.getIndex
    );

    router.route('/accounts/:idUseCase')
    .get(
        usersController.getToken,
        usersController.renderAccount
    );

    router.route('/accounts/:idUseCase')
    .get(
        usersController.getToken,
        usersController.renderAccount
    );

    router.route('/accounts/:idUseCase/transactions/:idAccount')
    .get(
        usersController.getToken,
        usersController.renderTransaction
    );

    router.route('/accounts/:idUseCase/balances/:idAccount')
    .get(
        usersController.getToken,
        usersController.renderBalance
    );

module.exports = router;