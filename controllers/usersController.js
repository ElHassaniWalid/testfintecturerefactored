const session = require('express-session');
const config = require('../config.json');
const fetch = require('node-fetch');
const views = require('../views/usersViews');
const qs = require("querystring");

const arkeaConf = config.dev.arkeon;

let getUrlAuthCode = (useCaseIdentity)=>{
  return arkeaConf.authorizeURL +"?response_type=code&client_id="+ arkeaConf.consumerKey +
  "&redirect_uri="+ arkeaConf.callbackURL +
  "&scope=aisp&psu="+useCaseIdentity;
};
let getRetrievAuthCode = async (url) => {

    return fetch(url)
    .then(resultat => {return resultat.url})
    .then(data => {
      let i = data.indexOf('=');
      let code = data.slice((i+1), data.length);
      return code;
    })
    .then(code => {return code;});
    
}
let getRefreshedToken = async (refreshToken)=>{
  let params  = { 
                  grant_type: 'refresh_token',
                  refresh_token: refreshToken
                };
  return  fetch( "https://api-sandbox.cmb.fr/oauth-authorizationcode-psd2/token" , {
                    method: 'post',
                    body:    qs.stringify(params),
                    port:    443,
                    headers: {
                    "cache-control": "no-cache",
                    "content-type": "application/x-www-form-urlencoded"
                  }
          })
          .then(result => result.json())
          .then(data =>  data);
}
let getTokenFromCode = async (code)=>{
  let params  = { 
                    client_id: config.dev.arkeon.consumerKey,
                    grant_type: 'authorization_code',
                    redirect_uri: config.dev.arkeon.callbackURL,
                    code: code 
                };
  return  fetch( "https://api-sandbox.cmb.fr/oauth-authorizationcode-psd2/token" , {
              method: 'post',
              body:    qs.stringify(params),
              port:    443,
              headers: {
                "cache-control": "no-cache",
                "content-type": "application/x-www-form-urlencoded"
              }
          })
          .then(result => result.json())
          .then(data =>  data);
}


let getDataAccounts = async (token, idUseCase)=>{ 
  return  fetch( "https://api-sandbox.cmb.fr/psd2/v1/accounts" , {
              method: 'get',
              port:    443,
              headers: {
                "accept": "application/hal+json; charset=utf-8",
                "Signature":"xxx",
                "X-Request-ID": "xxx",
                "Authorization": "Bearer "+token
              }
          })
          .then(result => result.json())
          .then(data =>  data);
}
let getDataTransaction = async (token, idAccount)=>{ 
  return  fetch( "https://api-sandbox.cmb.fr/psd2/v1/accounts/"+idAccount+"/transactions" , {
              method: 'get',
              port:    443,
              headers: {
                "accept": "application/hal+json; charset=utf-8",
                "Signature":"xxx",
                "X-Request-ID": "xxx", 
                "Authorization": "Bearer "+token
              }
          })
          .then(result => result.json())
          .then(data =>  data);
}

let getDataBalance = async (token, idAccount)=>{ 
  return  fetch( "https://api-sandbox.cmb.fr/psd2/v1/accounts/"+idAccount+"/balances" , {
              method: 'get',
              port:    443,
              headers: {
                "accept": "application/hal+json; charset=utf-8",
                "Signature":"xxx",
                "X-Request-ID": "xxx", 
                "Authorization": "Bearer "+token
              }
          })
          .then(result => result.json())
          .then(data =>  data);
}

module.exports = {
    getToken : async (req, res, next)=>{
        console.log('getToken Called !');
        console.log('get idUseCase from url ...');
        let id = req.params.idUseCase;
        let urlRetriveAuthCode = getUrlAuthCode(id);
        console.log('fetching retrieve code for idUseCase : '+ id);
        let retriveAuthCode = await getRetrievAuthCode(urlRetriveAuthCode); 
        console.log('fetching token for the retriveCode : '+ retriveAuthCode);
        let token = await getTokenFromCode(retriveAuthCode);
        req.session.token = token;
        req.session.idUseCase = id;
        next();
    },
    renderAccount : async (req, res, next)=>{
      let token = req.session.token.access_token;
      let idUseCase = req.session.idUseCase;
      
      let data = await getDataAccounts(token, idUseCase);
      return res.send(views.accountsView({idUseCase, data})).end();
    },
    renderTransaction : async (req, res, next)=>{
      let token = req.session.token.access_token;
      let idUseCase = req.session.idUseCase;
      let idAccount = req.params.idAccount;
      console.log('Token : '+token);
      let data = await getDataTransaction(token, idAccount);
      return res.send(views.transactionsView({idUseCase, data})).end();
    },
    renderBalance : async (req, res, next)=>{
      let token = req.session.token.access_token;
      let idUseCase = req.session.idUseCase;
      let idAccount = req.params.idAccount;
      console.log('Token : '+token);
      let data = await getDataBalance(token, idAccount);
      return res.send(views.balancesView({idUseCase, data})).end();
    },
    getIndex : async (req, res, next)=>{
      return res.send(views.indexView(arkeaConf.useCases)).end();
    }
    
}
